#include "tagview.h"

#include "fileview.h"
#include "tags_utils.h"
#include "types.h"

TagView::TagView(QObject *parent) : QAbstractTableModel(parent) {
	setObjectName("TagView");
}

Qt::ItemFlags TagView::flags(const QModelIndex &index) const {
	return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}

int TagView::rowCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);

	return static_cast<int>(Tags::N_Tags);
}

int TagView::columnCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);

	return 1;
}

bool TagView::setData(const QModelIndex &index, const QVariant &value, int role) {
	Q_UNUSED(role);

	if (TagsUtils::setTag(static_cast<Tags>(index.row()), value, file)) {
		updateTags();
		return true;
	}

	return false;
}

QVariant TagView::data(const QModelIndex &index, int role) const {
	if (!(role == Qt::DisplayRole || role == Qt::EditRole)) {
		return QVariant();
	}

	switch (index.column()) {
		case 0: {
			if (!tagslist.isEmpty()) {
				return tagslist.at(index.row());
			}

			break;
		}

		default: {
			qFatal("Unknown column in TagView!");
		}
	}

	return QVariant();
}

QVariant TagView::headerData(int section, Qt::Orientation orientation, int role) const {
	if (orientation != Qt::Vertical) {
		return QVariant();
	}

	if (role != Qt::DisplayRole) {
		return QVariant();
	}

	switch (static_cast<Tags>(section)) {
		case Tags::Title:
			return QVariant(tr("Title"));
			break;
		case Tags::Artist:
			return QVariant(tr("Artist"));
			break;
		case Tags::Album:
			return QVariant(tr("Album"));
			break;
		case Tags::Date:
			return QVariant(tr("Date"));
			break;
		case Tags::Comment:
			return QVariant(tr("Comment"));
			break;
		case Tags::Track:
			return QVariant(tr("Track"));
			break;
		case Tags::Genre:
			return QVariant(tr("Genre"));
			break;
		default:
			qFatal("Unknown column in TagView!");
			return QVariant();
	}
}

void TagView::on_itemSelected(const QString file) {
	this->file = file;
	updateTags();
}

void TagView::updateTags() {
	tagslist.clear();

	for (int i = 0; i < static_cast<int>(Tags::N_Tags); i++) {
		const QVariant tag = TagsUtils::getTag(static_cast<Tags>(i), file);
		tagslist.append(tag.toString());
	}

	emit dataChanged(index(0, 0), index(static_cast<int>(Tags::N_Tags) - 1, 0));
}
