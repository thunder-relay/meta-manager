#ifndef TYPES_H
#define TYPES_H

enum class Tags {
	Title,
	Artist,
	Album,
	Date,
	Comment,
	Track,
	Genre,
	N_Tags
};

enum class FileList {
	Nome
};

#endif // TYPES_H
