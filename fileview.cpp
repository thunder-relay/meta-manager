#include "fileview.h"

#include <QFileInfo>

FileView::FileView(QObject *parent) : QAbstractTableModel(parent) {
	setObjectName("FileView");
}

// Virtual functions.

int FileView::rowCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);
	return filelist.size();
}

int FileView::columnCount(const QModelIndex &parent) const {
	Q_UNUSED(parent);
	return 1;
}

QVariant FileView::data(const QModelIndex &index, int role) const {
	if (role != Qt::DisplayRole) {
		return QVariant();
	}

	const QString filename = filelist.at(index.row());
	const QFileInfo file(filename);

	switch (index.column()) {
		case 0:
			return file.fileName();
			break;
		default:
			qFatal("Unknown column in FileView!");
			return QVariant();
	}
}

QVariant FileView::headerData(int section, Qt::Orientation orientation, int role) const {
	Q_UNUSED(section);
	Q_UNUSED(orientation);
	Q_UNUSED(role);

	return QVariant();
}

// Other functions.

void FileView::addFiles(const QStringList files) {
	if (files.isEmpty()) {
		return;
	}

	foreach (const QString file, files) {
		if (!filelist.contains(file)) {
			beginInsertRows(QModelIndex(), rowCount(), rowCount());
			filelist.append(file);
			endInsertRows();
		}
	}

	if (!filelist.isEmpty()) {
		emit toggleRemoveButton(true);
	}
}

void FileView::removeFiles(const QList<QPersistentModelIndex> indexes) {
	if (indexes.isEmpty()) {
		return;
	}

	foreach (const QPersistentModelIndex &index, indexes) {
		beginRemoveRows(QModelIndex(), index.row(), index.row());

		removeRow(index.row());
		filelist.removeAt(index.row());

		endRemoveRows();
	}

	emit dataChanged(indexes.first(), indexes.last());

	if (filelist.isEmpty()) {
		emit toggleRemoveButton(false);
	}
}

QString FileView::getFile(const QModelIndex index) {
	if (!filelist.isEmpty()) {
		return filelist.at(index.row());
	}

	return QString();
}
