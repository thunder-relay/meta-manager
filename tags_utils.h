#ifndef TAGS_UTILS_H
#define TAGS_UTILS_H

#include "types.h"

#include <QDebug>
#include <QVariant>

#include <taglib/fileref.h>

class TagsUtils {
public:
	static QVariant getTag(const Tags tag, const QString filepath) {
		TagLib::FileRef fileref(filepath.toStdString().c_str());

		if (fileref.isNull()) {
			qWarning() << "Couldn't read the specified file.";
			return QVariant();
		}

		if (!fileref.tag()) {
			qWarning() << "Couldn't retrieve the specified file's tags.";
			return QVariant();
		}

		switch (tag) {
			case Tags::Title: {
				const TagLib::String title = fileref.tag()->title();
				return QVariant(TStringToQString(title));
			}

			case Tags::Artist: {
				const TagLib::String artist = fileref.tag()->artist();
				return QVariant(TStringToQString(artist));
			}

			case Tags::Album: {
				const TagLib::String album = fileref.tag()->album();
				return QVariant(TStringToQString(album));
			}

			case Tags::Date: {
				return QVariant(fileref.tag()->year());
			}

			case Tags::Comment: {
				const TagLib::String comment = fileref.tag()->comment();
				return QVariant(TStringToQString(comment));
			}

			case Tags::Track: {
				return QVariant(fileref.tag()->track());
			}

			case Tags::Genre: {
				const TagLib::String genre = fileref.tag()->genre();
				return QVariant(TStringToQString(genre));
			}

			default: {
				return QVariant();
			}
		}

		return QVariant();
	}

	static bool setTag(const Tags tag, const QVariant value, const QString filepath) {
		TagLib::FileRef fileref(filepath.toStdString().c_str());

		if (fileref.isNull()) {
			qWarning() << "Couldn't read the specified file.";
			return false;
		}

		if (!fileref.tag()) {
			qWarning() << "Couldn't retrieve the specified file's tags.";
			return false;
		}

		switch (tag) {
			case Tags::Title: {
				fileref.tag()->setTitle(QStringToTString(value.toString()));
				break;
			}

			case Tags::Artist: {
				fileref.tag()->setArtist(QStringToTString(value.toString()));
				break;
			}

			case Tags::Album: {
				fileref.tag()->setAlbum(QStringToTString(value.toString()));
				break;
			}

			case Tags::Date: {
				fileref.tag()->setYear(value.toUInt());
				break;
			}

			case Tags::Comment: {
				fileref.tag()->setComment(QStringToTString(value.toString()));
				break;
			}

			case Tags::Track: {
				fileref.tag()->setTrack(value.toUInt());
				break;
			}

			case Tags::Genre: {
				fileref.tag()->setGenre(QStringToTString(value.toString()));
				break;
			}

			default: {
				return false;
			}
		}

		return fileref.save();
	}
};

#endif // TAGS_UTILS_H
