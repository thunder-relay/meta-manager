#ifndef FILEVIEW_H
#define FILEVIEW_H

#include <QAbstractTableModel>

class FileView : public QAbstractTableModel {
	Q_OBJECT

private:
	QStringList filelist;

public:
	FileView(QObject *parent = 0);

	 // Pure virtual methods.
	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QVariant data(const QModelIndex &index = QModelIndex(), int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	void addFiles(const QStringList files);
	void removeFiles(const QList<QPersistentModelIndex> indexes);
	QString getFile(const QModelIndex index);

signals:
	void toggleRemoveButton(bool enabled);
};

#endif // FILEVIEW_H
