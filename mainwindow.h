#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QModelIndex>

namespace Ui {
class MainWindow;
}

class FileView;
class TagView;
class QSettings;
class QTranslator;

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_button_add_clicked();
	void on_button_remove_clicked();
	void on_table_files_clicked(const QModelIndex index);
	void on_action_english_triggered();
	void on_action_italian_triggered();
	void on_action_use_last_opened_folder_triggered();
	void on_action_use_custom_folder_triggered();
	void on_action_light_triggered();
	void on_action_dark_triggered();
	void on_action_about_qt_triggered();

public slots:
	void toggleRemoveButton(bool enabled);

signals:
	void itemSelected(const QString file);

private:
	Ui::MainWindow *ui;
	FileView *fileview;
	TagView *tagview;
	QSettings *settings;
	QTranslator *translator;

	void changeEvent(QEvent *e) Q_DECL_OVERRIDE;
	void initializeActionGroups();
	void initializeSettings();
	void loadSettings();
};

#endif // MAINWINDOW_H
