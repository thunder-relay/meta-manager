<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en_GB">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Meta Manager</source>
        <translation>Meta Manager</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="31"/>
        <source>Add files to the list</source>
        <translation>Aggiungi file alla lista</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="54"/>
        <source>Remove file from the list</source>
        <translation>Rimuovi file dalla lista</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="131"/>
        <source>Options</source>
        <translation>Opzioni</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="135"/>
        <location filename="../../mainwindow.ui" line="169"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="142"/>
        <source>Default folder</source>
        <translation>Cartella predefinita</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="149"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="160"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="177"/>
        <source>Dark</source>
        <translation>Scuro</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="188"/>
        <source>Light</source>
        <translation>Chiaro</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="196"/>
        <source>Use custom folder</source>
        <translation>Utilizza cartella personalizzata</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="207"/>
        <source>Use last opened folder</source>
        <translation>Utilizza l&apos;ultima cartella visualizzata</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="218"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="226"/>
        <source>Italiano</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="231"/>
        <source>About Qt...</source>
        <translation>Riguardo Qt...</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="126"/>
        <source>Select one or more files</source>
        <translation>Seleziona uno o più file</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="177"/>
        <source>Select a folder to use as the default location</source>
        <translation>Seleziona una cartella da usare come posizione predefinita</translation>
    </message>
</context>
<context>
    <name>TagView</name>
    <message>
        <location filename="../../tagview.cpp" line="71"/>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <location filename="../../tagview.cpp" line="74"/>
        <source>Artist</source>
        <translation>Artista</translation>
    </message>
    <message>
        <location filename="../../tagview.cpp" line="77"/>
        <source>Album</source>
        <translation>Album</translation>
    </message>
    <message>
        <location filename="../../tagview.cpp" line="80"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../tagview.cpp" line="83"/>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <location filename="../../tagview.cpp" line="86"/>
        <source>Track</source>
        <translation>Traccia</translation>
    </message>
    <message>
        <location filename="../../tagview.cpp" line="89"/>
        <source>Genre</source>
        <translation>Genere</translation>
    </message>
</context>
</TS>
