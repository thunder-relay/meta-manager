#ifndef TAGVIEW_H
#define TAGVIEW_H

#include <QAbstractTableModel>

class FileView;

class TagView : public QAbstractTableModel
{
	Q_OBJECT

public:
	TagView(QObject *parent = 0);

	// Pure virtual methods.
	int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
	QVariant data(const QModelIndex &index = QModelIndex(), int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
	QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

	// Virtual methods.
	bool setData(const QModelIndex &index, const QVariant &value, int role) Q_DECL_OVERRIDE;
	Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;

public slots:
	void on_itemSelected(const QString file);

private:
	QString file;
	QStringList tagslist;
	void updateTags();
};

#endif // TAGVIEW_H
