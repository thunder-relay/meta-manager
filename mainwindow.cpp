#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "fileview.h"
#include "tagview.h"

#include <QFileDialog>
#include <QFileInfo>
#include <QSettings>
#include <QStyleFactory>
#include <QTranslator>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), fileview(new FileView(this)), tagview(new TagView(this)),
	settings(new QSettings("config.ini", QSettings::IniFormat, this)), translator(new QTranslator(this)) {

	setObjectName("MainWindow");

	ui->setupUi(this);
	ui->table_files->setModel(fileview);
	ui->table_tags->setModel(tagview);

	initializeActionGroups();
	initializeSettings();
	loadSettings();

	qApp->installTranslator(translator);

	connect(fileview, SIGNAL(toggleRemoveButton(bool)), this, SLOT(toggleRemoveButton(bool)));
	connect(this, SIGNAL(itemSelected(const QString)), tagview, SLOT(on_itemSelected(const QString)));
}

MainWindow::~MainWindow() {
	delete ui;
}

void MainWindow::changeEvent(QEvent *e) {
	if (e->type() == QEvent::LanguageChange) {
		ui->retranslateUi(this);
	}

	QMainWindow::changeEvent(e);
}

void MainWindow::initializeActionGroups() {
	QActionGroup *themeActionGroup = new QActionGroup(this);
	themeActionGroup->addAction(ui->action_dark);
	themeActionGroup->addAction(ui->action_light);

	QActionGroup *folderActionGroup = new QActionGroup(this);
	folderActionGroup->addAction(ui->action_use_custom_folder);
	folderActionGroup->addAction(ui->action_use_last_opened_folder);

	QActionGroup *languageActionGroup = new QActionGroup(this);
	languageActionGroup->addAction(ui->action_english);
	languageActionGroup->addAction(ui->action_italian);
}

void MainWindow::initializeSettings() {
	if (!settings->contains("Language")) {
		const QString language = QLocale::languageToString(QLocale::system().language());
		settings->setValue("Language", language);
	}

	if (!settings->contains("CustomFolder")) {
		settings->setValue("CustomFolder", "");
	}

	if (!settings->contains("DefaultFolder")) {
		settings->setValue("DefaultFolder", "last");
	}

	if (!settings->contains("LastFolder")) {
		settings->setValue("LastFolder", "");
	}

	if (!settings->contains("Theme")) {
		settings->setValue("Theme", "light");
	}
}

void MainWindow::loadSettings(){
	const QVariant language = settings->value("Language");
	if (language == "italian") {
		on_action_italian_triggered();
		ui->action_italian->setChecked(true);
	}

	const QVariant folder = settings->value("DefaultFolder");
	if (folder == "custom") {
		ui->action_use_custom_folder->setChecked(true);
	}

	const QVariant theme = settings->value("Theme");
	if (theme == "dark") {
		on_action_dark_triggered();
		ui->action_dark->setChecked(true);
	}
}

void MainWindow::toggleRemoveButton(bool enabled) {
	ui->button_remove->setEnabled(enabled);
}

void MainWindow::on_button_add_clicked() {
	const QString default_folder_option = settings->value("DefaultFolder").toString();
	QString path;

	bool use_custom_folder = false;
	if (default_folder_option == "custom") {
		path = settings->value("CustomFolder").toString();
		QDir directory(path);

		if (directory.exists()) {
			use_custom_folder = true;
		}
	}

	if (!use_custom_folder) {
		path = settings->value("LastFolder").toString();
	}

	QStringList filters;
	filters << "audio/flac" << "audio/mp3";

	QFileDialog dialog;
	dialog.setWindowTitle(tr("Select one or more files"));
	dialog.setDirectory(path);
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setMimeTypeFilters(filters);

	if (!dialog.exec()) {
		return;
	}

	const QStringList list = dialog.selectedFiles();

	fileview->addFiles(list);

	const QFileInfo fileinfo(list.first());
	settings->setValue("LastFolder", fileinfo.canonicalPath());
}

void MainWindow::on_button_remove_clicked() {
	QList<QPersistentModelIndex> persistentModelIndexes;

	foreach (const QModelIndex &index, ui->table_files->selectionModel()->selectedRows()) {
		persistentModelIndexes << index;
	}

	fileview->removeFiles(persistentModelIndexes);
}

void MainWindow::on_table_files_clicked(const QModelIndex index) {

	const QString file = fileview->getFile(index);

	emit itemSelected(file);
}

void MainWindow::on_action_english_triggered() {
	settings->setValue("Language", "english");
	translator->load(":/languages/english.qm");
}

void MainWindow::on_action_italian_triggered() {
	settings->setValue("Language", "italian");
	translator->load(":/languages/italian.qm");
}

void MainWindow::on_action_use_last_opened_folder_triggered() {
	settings->setValue("DefaultFolder", "last");
}

void MainWindow::on_action_use_custom_folder_triggered() {
	settings->setValue("DefaultFolder", "custom");

	const QString path = QFileDialog::getExistingDirectory(this, tr("Select a folder to use as the default location"), settings->value("CustomFolder").toString());

	if (!path.isEmpty()) {
		settings->setValue("CustomFolder", path);
	}
}

void MainWindow::on_action_light_triggered() {
	qApp->setPalette(style()->standardPalette());

	settings->setValue("Theme", "light");
}

void MainWindow::on_action_dark_triggered() {
	QPalette palette;
	palette.setColor(QPalette::Window, QColor(53,53,53));
	palette.setColor(QPalette::WindowText, Qt::white);
	palette.setColor(QPalette::Base, QColor(15,15,15));
	palette.setColor(QPalette::AlternateBase, QColor(53,53,53));
	palette.setColor(QPalette::ToolTipBase, Qt::white);
	palette.setColor(QPalette::ToolTipText, Qt::white);
	palette.setColor(QPalette::Text, Qt::white);
	palette.setColor(QPalette::Button, QColor(53,53,53));
	palette.setColor(QPalette::ButtonText, Qt::white);
	palette.setColor(QPalette::BrightText, Qt::red);

	palette.setColor(QPalette::Highlight, QColor(15,60,90).lighter());
	palette.setColor(QPalette::HighlightedText, Qt::black);

	qApp->setPalette(palette);

	settings->setValue("Theme", "dark");
}

void MainWindow::on_action_about_qt_triggered()
{
	qApp->aboutQt();
}
